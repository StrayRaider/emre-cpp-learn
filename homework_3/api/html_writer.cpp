#ifndef HTML_WRITER_H_
#define HTML_WRITER_H_

#include <string>

namespace html_writer {

    void OpenDocument(){

    }

    void CloseDocument(){

    }

    void AddCSSStyle(const std::string& stylesheet){

    }

    void AddTitle(const std::string& title){

    }

    void OpenBody(){

    }

    void CloseBody(){

    }

    void OpenRow(){

    }

    void CloseRow(){

    }

    void AddImage(const std::string& img_path, float score, bool highlight = false){
        
    }

}

#endif