#ifndef IMAGE_BROWSER_HPP_
#define IMAGE_BROWSER_HPP_

#include <array>
#include <string>
#include <tuple>
#include <vector>

namespace image_browser {

using ScoredImage = std::tuple<std::string, float>;
using ImageRow = std::array<ScoredImage, 3>;

void AddFullRow(const ImageRow& row, bool first_row = false){}

void CreateImageBrowser(const std::string& title, const std::string& stylesheet,
                        const std::vector<ImageRow>& rows){}

}
#endif