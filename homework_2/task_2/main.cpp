#include <iostream>
#include <string.h>

using namespace std;


int main(int argc, char *argv[]) {
	char *val_1, *val_2, *number_1, *number_2;
	char *ext_1, *ext_2;
  
	if (argc-1 != 2)
		fprintf(stderr, "Two input arguments are needed \n");
  
	else {
		val_1 = strtok(argv[1], ".");
		while (val_1 != NULL){
			val_1 = strtok(NULL, ".");
			if (val_1 != NULL) 
				ext_1 = val_1;
		}
		number_1 = strtok(argv[1], ".");

		val_2 = strtok(argv[2], ".");
		while (val_2 != NULL){
			val_2 = strtok(NULL, ".");
			if (val_2 != NULL) 
				ext_2 = val_2;
		}
		number_2 = strtok(argv[2], ".");

		if (!strcmp(ext_1, "txt") && !strcmp(ext_2, "txt"))
			cout << (atoi(number_1) + atoi(number_2)) / 2 << endl;
		else if (!strcmp(ext_1, "png") && !strcmp(ext_2, "png"))
			cout << atoi(number_1) + atoi(number_2) << endl;
		else if ((!strcmp(ext_1, "txt") && !strcmp(ext_2, "png")) || (!strcmp(ext_1, "png") && !strcmp(ext_2, "txt")))
			cout << atoi(number_1) % atoi(number_2) << endl;
		else
			fprintf(stderr, "Invalid arguman. \n ");
	}
	return 0;
}