#include<iostream>
#include <random>
#include <cstdlib>

using namespace std;

int main(){
	bool guested = false;
	int usr_number =0;

	std::random_device rd;
	std::uniform_int_distribution<int> dist(0, 99);
	int target = dist(rd);

	//std::cout << target << std::endl;
	cout<< "guess the number :" <<endl;
	while(!guested){
		std::cin >> usr_number;
		if(0 > usr_number || usr_number > 99)
			fprintf(stderr, "“[WARNING] : Number must be between 0 and 99” \n");
		else if(std::cin.fail()){
			fprintf(stderr, "“Error encountered, exiting...” \n");
			exit(EXIT_FAILURE);
		}

		else{
			if(usr_number == target){
				cout<< "You Won ! " <<endl;
				guested = true;
			}
			else if(usr_number > target){
				cout<< "The target is smaller " <<endl;
			}
			else if(usr_number < target){
				cout<< "The target is bigger " <<endl;
			}
		}
	}
	std::cout << "The number is : "<< target << std::endl;
	return 0;
}